# FoodTrucks
About Project
  * Simple example of how to create a REST API with Phoenix
  * Simple example of how to create a CRUD with Phoenix Liveview
  * Use Amnesia as DB Manager(All query are strict)
  * Use Surface as Template Manager
  * This example was create as responsive template using TailwindCSS

# Installation

In order to build a Phoenix application, we will need a few dependencies installed in our Operating System:

  * The Erlang VM and the Elixir programming language
  * Elixir 1.14 or later


To start your Phoenix server:
  * Run `mix deps.get` to install dependencies
  * Run `mix deps.compile` to compile dependencies
  * Start Phoenix endpoint inside IEx with `iex -S mix phx.server`
  * Run command `FoodTrucks.mnesia_setup()` to populate the DB


Web view ( it's just a simple report page )
   * you can visit [`localhost:4000`](http://localhost:4000) from your browser.


API 
  * add_food_truck (PUT) ---------------- > (insert row data to DB) 
  * get_food_truck (GET) ---------------- > (get row data from DB) 
  * delete_food_truck (DELETE) ---------------- > ( delete row from DB) 
  * update_food_truck (PATCH) ---------------- > (update some cell of row DB) 
  * search_filters (POST) ---------------- > (make a search by params on DB) 

API Examples
  * add_food_truck


    $ curl --location --request PUT 'http://localhost:4000/api/add_food_truck' \
           --header 'Content-Type: application/json' \
           --data '{
                     "locationid": 8605022,
                     "applicant": "ACR",
                     "facilitytype": "Truck",
                     "locationdescription": "TAYLOR ST: BAY ST to NORTH POINT ST (2500 - 2599)",
                     "address": "2535 TAYLOR ST",
                     "permit": "21MFF-00106",
                     "status": "APPROVED",
                     "fooditems": "Asian Fusion - Japanese Sandwiches/Sliders/Misubi",
                     "x": 6008186.35457,
                     "y": 2121568.81783,
                     "latitude": 37.805885350100986,
                     "longitude": -122.41594524663745,
                     "dayshours": "",
                     "approved": "11/05/2021 12:00:00 AM",
                     "expirationdate": "11/15/2022 12:00:00 AM",
                     "location": "(37.805885350100986, -122.41594524663745)",
                     "fire_prevention_districts": 5,
                     "zip_codes": "308"
                   }'
  
  * get_food_truck
    
        $ curl --location 'http://localhost:4000/api/get_food_truck/8605022'

  * delete_food_truck
       
        $ curl --location --request DELETE 'http://localhost:4000/api/delete_food_truck/8605022'

  * update_food_truck

        $ curl --location --request PATCH 'http://localhost:4000/api/update_food_truck/8605022' \
               --header 'Content-Type: application/json' \
               --data '{
                          "applicant": "Modified Applicant",
                          "facilitytype": "Other"
                       }'

  * search_filters
        
        $ curl --location 'http://localhost:4000/api/search_filters' \
                --header 'Content-Type: application/json' \
                --data '{
                           "zip_codes": "308",
                           "facilitytype": "Other",
                        }'




## Learn more about Phoenix Framework

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix


## Learn more about Surface

* Official website: https://surface-ui.org/
* 

## Learn more about TailwindCss

* Official website: https://tailwindcss.com/
