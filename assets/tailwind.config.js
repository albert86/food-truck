// See the Tailwind configuration guide for advanced usage
// https://tailwindcss.com/docs/configuration

const plugin = require("tailwindcss/plugin")
const fs = require("fs")
const path = require("path")

module.exports = {
  content: [
    "./js/**/*.js",
    "../lib/food_trucks_web.ex",
    "../lib/food_trucks_web/**/*.*ex",
    "../lib/food_trucks_web/**/*.sface",
    "../priv/catalogue/**/*.{ex,sface}"
  ],
  theme: {
    extend: {
      colors: {
        brand: "#FD4F00",
        box: "#adade6",
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    // Allows prefixing tailwind classes with LiveView classes to add rules
    // only when LiveView classes are applied, for example:
    //
    //     <div class="phx-click-loading:animate-ping">
    //
    plugin(({addVariant}) => addVariant("phx-no-feedback", [".phx-no-feedback&", ".phx-no-feedback &"])),
    plugin(({addVariant}) => addVariant("phx-click-loading", [".phx-click-loading&", ".phx-click-loading &"])),
    plugin(({addVariant}) => addVariant("phx-submit-loading", [".phx-submit-loading&", ".phx-submit-loading &"])),
    plugin(({addVariant}) => addVariant("phx-change-loading", [".phx-change-loading&", ".phx-change-loading &"])),

    // Embeds Heroicons (https://heroicons.com) into your app.css bundle
    // See your `CoreComponents.icon/1` for more information.
    //
    plugin(function({matchComponents, theme}) {
      let iconsDir = path.join(__dirname, "./vendor/heroicons/optimized")
      let values = {}
      let icons = [
        ["", "/24/outline"],
        ["-solid", "/24/solid"],
        ["-mini", "/20/solid"]
      ]
      icons.forEach(([suffix, dir]) => {
        fs.readdirSync(path.join(iconsDir, dir)).forEach(file => {
          let name = path.basename(file, ".svg") + suffix
          values[name] = {name, fullPath: path.join(iconsDir, dir, file)}
        })
      })
      matchComponents({
        "hero": ({name, fullPath}) => {
          let content = fs.readFileSync(fullPath).toString().replace(/\r?\n|\r/g, "")
          return {
            [`--hero-${name}`]: `url('data:image/svg+xml;utf8,${content}')`,
            "-webkit-mask": `var(--hero-${name})`,
            "mask": `var(--hero-${name})`,
            "mask-repeat": "no-repeat",
            "background-color": "currentColor",
            "vertical-align": "middle",
            "display": "inline-block",
            "width": theme("spacing.5"),
            "height": theme("spacing.5")
          }
        }
      }, {values})
    }),
    plugin(function ({addUtilities}) {
      addUtilities({
        '.btn': {
          'cursor': 'pointer',
          'display': 'inline-flex',
          'justify-content': 'center',
          'padding': '0.56rem',
          'font-size': '0.875rem',
          'line-height': 'initial',
          'font-weight': '500',
          'border-radius': '0.375rem',
          'min-width': '106px',
          'align-items': 'center',
          'height': '30px',

          '&.btn-primary': {
            'border': '2px transparent solid',
            'background-color': '#004393',
            'color': 'white',
          },

          '&.btn-secondary': {
            'border': '1px #004393 solid',
            'color': '#004393',
            'background-color': 'white',
          },

          '&:disabled': {
            'opacity': '0.7',
            'cursor': 'default',
          }
        },
        '.input-group': {
          position: 'relative'
        },
        '.input-group_input': {
          'background-color': 'transparent',
          transition: 'outline-color 500ms'
        },
        '.input-group_input:is(focus,:valid), .input-group_input.valid, .date-picker-border.enabled.valid': {
          'outline-color': 'var(--label_valid-outline-color)',
          'border': 'solid 1px',
          'border-color': 'var(--label_valid-outline-color)'
        },
        '.input-group_input:is(focus,:invalid), .input-group_input.invalid, .date-picker-border.enabled.invalid': {
          'outline-color': 'var(--label_invalid-outline-color)',
          'border': 'solid 1px',
          'border-color': 'var(--label_invalid-outline-color)'
        },
        '.input-group_label': {
          position: 'absolute',
          top:'var(--label_top)',
          left: '19px',
          translate: '2px 10px',
          color: '#625a5a',
          transition: 'translate 500ms, scale 500ms',
          'font-size': '0.89rem',
          padding: '0 10px'
        },
        '.input-group_input.enabled:focus + .input-group_label, .input-group_input.enabled.valid + .input-group_label, .date-picker-border.enabled.valid .input-group_label, .enabled .date-input-group_input:focus + .input-group_label': {
          'padding-inline': '1px',
          translate: '2px -8px',
          scale: 'var(--label_scale)',
          background: 'linear-gradient(0deg, var(--label_bg_color) 50%, #ffffff00 100%)',
          'border-top-left-radius': '0.5rem',
          'border-top-right-radius': '0.5rem',
          padding: '0 5px',
        },
        '.input-group_input[readonly], .input-group_input[disabled]':
            {
              'outline-color': 'var(--label_read-only-color) !important',
              'border': 'solid 1px',
              'border-color':  'var(--label_read-only-color) !important',
            },
        ".border-gradient" : {
          "border-bottom": "2px solid transparent",
          "border-image": "linear-gradient(0.25turn, var(--border-gradient-color-1), var(--border-gradient-color-2), var(--border-gradient-color-3))",
          "border-image-slice": "1",
          "width": "100%",
        },


      })
    }),
    require('@tailwindcss/forms')
  ]
}
