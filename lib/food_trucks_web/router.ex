defmodule FoodTrucksWeb.Router do
  use FoodTrucksWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {FoodTrucksWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", FoodTrucksWeb do
    pipe_through :api

#    post "/search_filters/", ApiController, :search_filters_m
    post "/search_filters/", ApiController, :search_filters
    put "/add_food_truck/", ApiController, :add_food_truck
    get "/get_food_truck/:locationid", ApiController, :get_food_truck
    delete "/delete_food_truck/:locationid", ApiController, :delete_food_truck
    patch "/update_food_truck/:locationid", ApiController, :update_food_truck

  end

  scope "/", FoodTrucksWeb do
    pipe_through :browser

    live "/", LivePage.ReportLive
  end

  # Other scopes may use custom stacks.
  # scope "/api", FoodTrucksWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:food_trucks, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: FoodTrucksWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
