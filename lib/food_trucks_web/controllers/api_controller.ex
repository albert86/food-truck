defmodule FoodTrucksWeb.ApiController do
  use FoodTrucksWeb, :controller

  import FoodTrucks.Api, only: [get_filters_result: 1, add_food_truck: 1, get_food_truck: 1, delete_food_truck: 1, update_food_truck_m: 2]

  def search_filters(conn, params) do
      result =
        case Enum.filter(params,&form_data_empty_filter/1) |> validate_params() do
          {:ok, value} -> get_filters_result(value)

          {:error, value} -> "error on payload #{value.error}"
        end
        httpResponse(conn, 200, %{"success" => true, "response" => result })
  end

  def add_food_truck(conn, params) do with {:ok, truck_data} <- {:ok, Enum.map(params,&form_data_value_mapper/1) |> Map.new()}
    do
      add_food_truck(truck_data)
      httpResponse(conn, 200, %{"success" => true, "response" =>  "Food Truck was inserted"})
    end
  end

  def get_food_truck(conn, %{"locationid" => locationid }) do
    httpResponse(conn, 200, %{"success" => true, "response" =>  get_food_truck(locationid)})
  end

  def delete_food_truck(conn, %{"locationid" => locationid }) do
    httpResponse(conn, 200, %{"success" => true, "response" =>  delete_food_truck(locationid)})
  end

  def update_food_truck(conn, %{"locationid" => locationid } = data) do
    params = Map.drop(data,["locationid"])
    result =
      case validate_params(params) do
        {:ok, value} -> update_food_truck_m(locationid,Map.new(value))

        {:error, value} -> "error on payload #{value.error}"
      end
    httpResponse(conn, 200, %{"success" => true, "response" => result })
  end

  def httpResponse(conn, status, data) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json; charset=utf-8")
    |> Plug.Conn.send_resp(status, Poison.encode!(data, pretty: true))
  end

  defp validate_params(params) do
    mapper = Enum.map(params, &form_data_value_mapper/1)
    case Enum.find(mapper, fn val -> find_error(val) end) do
      nil -> {:ok, mapper}

      value -> {:error, value}
    end
  end

  defp find_error(%{:error => _}), do: true
  defp find_error(_), do: false

  defp form_data_value_mapper({"locationid" = k, v}) when is_bitstring(v), do: { String.to_atom(k), String.to_integer(v) }
  defp form_data_value_mapper({"locationid" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"applicant" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"facilitytype" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"locationdescription" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"address" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"permit" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"status" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"fooditems" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"x" = k, v})  when is_bitstring(v), do: { String.to_atom(k), String.to_float(v) }
  defp form_data_value_mapper({"x" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"y" = k, v} )when is_bitstring(v), do: { String.to_atom(k), String.to_float(v) }
  defp form_data_value_mapper({"y" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"latitude" = k, v}) when is_bitstring(v), do: { String.to_atom(k), String.to_float(v) }
  defp form_data_value_mapper({"latitude" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"longitude" = k, v}) when is_bitstring(v), do: { String.to_atom(k), String.to_float(v) }
  defp form_data_value_mapper({"longitude" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"dayshours" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"approved" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"expirationdate" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"location" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"fire_prevention_districts" = k, v}), do: { String.to_atom(k), v }
  defp form_data_value_mapper({"zip_codes" = k, v}), do: { String.to_atom(k), String.to_integer(v) }
  defp form_data_value_mapper({k,_}), do: %{:error => k}

  defp form_data_empty_filter({_,v}), do: v != ""

end