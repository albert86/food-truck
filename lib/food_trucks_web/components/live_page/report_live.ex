defmodule FoodTrucksWeb.LivePage.ReportLive do
  use Surface.LiveView
  use FoodTrucksWeb, :verified_routes
  import AppTaskSupervisor, only: [call_task_supervisor: 1]
  alias Surface.Components.Form
  alias FoodTrucksWeb.LiveComponents.{Input, ResultCard}

  @impl true
  def mount(_params, session, socket) do

#    Subscribe to PubSub to receive de msg when form was submitted
    AppTaskSupervisor.subscribe(session["_csrf_token"])

    {:ok,
      assign(socket, :is_searching, false)
      |> assign(:form_data, %{})
      |> assign(:result, [])
      |> assign(:session_id, session["_csrf_token"])
    }
  end

  @impl true
  def handle_event("change_search_form", form_data, socket) do
    {:noreply, assign(socket, form_data: form_data) }
  end

  def handle_event("submit_form", %{"value" => encode_form_data}, socket) do
    object_map =
      Jason.decode!(encode_form_data)
      |> Map.drop(["_csrf_token", "_target" ])
      |> Enum.filter(&form_data_empty_filter/1)
      |> Enum.map(&form_data_to_atom/1)
      |> Enum.map(&form_data_value_mapper/1)

    # Calling the TaskSupervisor to raise the search in another thread and not time_out the socket
    call_task_supervisor(%{"generate-report" => %{"filters" => object_map, "session_id" => socket.assigns.session_id}})
    {:noreply, socket }
  end

  def handle_event("submit_form", _, socket), do: {:noreply, socket }

  @impl true
  def handle_info({:report_result, data}, socket), do: {:noreply, assign(socket, :is_searching, true) |> assign(:result, data) }

  defp form_data_to_atom({k,v}), do: { String.replace(k,"-input","") |> String.to_atom(), v }

  defp form_data_value_mapper({:zip_codes = k ,v}), do: { k, String.to_integer(v) }
  defp form_data_value_mapper(value), do: value

  defp form_data_empty_filter({_,v}), do: v != ""

end
