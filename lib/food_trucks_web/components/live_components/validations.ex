defmodule FoodTrucksWeb.LiveComponents.Validations do
  @moduledoc false

  def is_valid(nil, "required"), do: false
  def is_valid("", "required"), do: false
  def is_valid("nil", "required"), do: false
  def is_valid(_, "required"), do: true


  def is_valid(row_value, validations) when is_list(validations) do
    Enum.map(validations, & is_valid(row_value, &1))
    |> Enum.find(& &1 == false)
    |> case  do
        nil -> true
        _ -> false
      end
  end

  def is_valid(_,_), do: true

  def validation_class(row_value, validations) do
    cond do
      is_valid(row_value,validations) -> "valid"
      true -> "invalid"
    end
  end

  def enabled_class(true), do: "enabled"
  def enabled_class(_), do: ""

end
