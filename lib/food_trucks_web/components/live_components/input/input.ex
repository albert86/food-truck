defmodule FoodTrucksWeb.LiveComponents.Input do
  use Surface.LiveComponent

  import FoodTrucksWeb.LiveComponents.Validations, only: [is_valid: 2]

  prop input_label, :string, required: true
  prop input_value, :string, default: ""
  prop input_width, :string, default: "1/3"
  prop validations, :list, default: ["required"]

  def handle_event("input_keyup", %{"value" => value}, socket) do
    {:noreply, assign(socket, input_value: value)}
  end
end
