defmodule FoodTrucksWeb.LiveComponents.ResultCard do
  use Surface.LiveComponent

  prop truck_data, :map, default: %{}

  defp mapper(index, map), do: Map.get(map, index,"")

end
