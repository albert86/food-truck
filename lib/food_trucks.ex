defmodule FoodTrucks do
  @moduledoc """
  FoodTrucks keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  require Logger

  alias FoodTrucks.{Database.Trucks, Database.Query}

  @doc """
  Read data from the JSON file to populate the DB
    - Start this function at first running of project
  """
  def mnesia_setup do
    Amnesia.stop
    Amnesia.Schema.destroy
    Amnesia.Schema.create([node()])
    Amnesia.start
    FoodTrucks.Database.create(disk: [node()])
    file_path = download_file_path("data.json")
    case File.read(file_path) do
      {:ok, read } ->
           insert_to_db = Jason.decode!(read)
           |> Enum.map(&json_data_to_database/1)
           |> Query.new()
           Logger.info("population result #{inspect insert_to_db }")

      _ -> Logger.error("missed file #{file_path}")
    end
  end

  defp json_data_to_database(data) do
    %Trucks{
      :locationid => data["locationid"],
      :applicant => data["Applicant"],
      :facilitytype => data["FacilityType"],
      :locationdescription => data["LocationDescription"],
      :address => data["Address"],
      :permit => data["permit"],
      :status => data["Status"],
      :fooditems => data["FoodItems"],
      :x => data["X"],
      :y => data["Y"],
      :latitude => data["Latitude"],
      :longitude => data["Longitude"],
      :dayshours => data["dayshours"],
      :approved => data["Approved"],
      :expirationdate => data["ExpirationDate"],
      :location => data["Location"],
      :fire_prevention_districts => data["Fire Prevention Districts"],
      :zip_codes => data["Zip Codes"]
    }
  end

  defp download_file_path(file_name), do: "#{:code.priv_dir(:food_trucks) }/config_data/#{file_name}"

end
