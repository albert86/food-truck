defmodule FoodTrucks.Database.Query do
  use Amnesia

  # match_keyword: [name: "rol1"]
  def get(table, match_keyword) do
    Amnesia.transaction do
      table.match(match_keyword)
      |> Amnesia.Selection.values()
    end
  end

  def get_one(table, match_keyword) do
    case get(table, match_keyword) do
      [head | _tail] -> head
      _ -> table.__struct__()
    end
  end

  def new(records) when is_list(records), do: Enum.each(records, &new/1)
  def new(record) do
    Amnesia.transaction do
      record |> record.__struct__.write()
    end
  end

  def alter(module, params) do
    Amnesia.transaction do
      Map.merge(
        module,
        module
        |> Map.from_struct()
        |> Map.merge(params)
        ) |> module.__struct__.write()
    end
  end

  def update_keys(module, get_params, keys, data) do
    Amnesia.transaction do
      elements = module.match(get_params)
      |> Amnesia.Selection.values()

      Enum.each(elements, fn elem ->
        payload = Enum.reduce(keys, elem, fn key, acc ->
          new_data = Map.get(data, key)
          Map.merge(acc, %{key => new_data})
        end)
        alter(elem, payload)
      end)
    end
  end

  def update_key(module, get_params, key, data) do
    Amnesia.transaction do
      elements = module.match(get_params)
      |> Amnesia.Selection.values()

      Enum.each(elements, fn elem ->
        payload = Map.get(elem, key) |> Map.merge(data)
        alter(elem, %{key => payload})
      end)
    end
  end

#  Firmarepresentantes.Database.Query.get(Firmarepresentantes.Database.Notification,[type: :sign_doc]) |> Firmarepresentantes.Database.Query.remove()
  def remove(modules) do
    Amnesia.transaction do
      List.foldr(modules, [], fn entry, acc ->
        [
          entry.__struct__.delete(entry)
          |> is_deleted(entry)
          | acc
        ]
      end)
    end
  end
#  Firmarepresentantes.Database.Query.remove(Firmarepresentantes.Database.Notification,[type: :sign_doc])
  def remove(from_module, get_criteria), do: get(from_module,get_criteria) |> remove()

  def to_struct([head | tail]) do
    Map.merge(
      struct(head),
      Amnesia.Table.info(head, :attributes)
      |> Enum.zip(tail)
      |> Map.new()
    )
  end

  defp is_deleted(:ok, entry), do: entry
  defp is_deleted(_ignore, _entry), do: nil
end
