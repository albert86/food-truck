require Amnesia
use Amnesia

defdatabase FoodTrucks.Database do
  deftable Trucks, [:locationid, :applicant, :facilitytype,:locationdescription, :address, :permit, :status, :fooditems, :x, :y, :latitude, :longitude, :dayshours, :approved, :expirationdate, :location, :fire_prevention_districts, :zip_codes], type: :set

end
