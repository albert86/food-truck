defmodule AppTaskSupervisor do
  use Task
  require Logger

  import FoodTrucks.Api, only: [get_filters_result: 1]
  alias FoodTrucks.PubSub

  @topic inspect(__MODULE__)

  def start_link(arg), do: Task.start_link(__MODULE__, :run, [arg])


  def run(%{"generate-report" => data}), do: generate_report(data) |> notify_subscribers()
  def run(_), do: Logger.info("--------------------------- AppTaskSupervisor not_defined --------------")

  defp notify_subscribers(%{"report-result" => rows, "session_id" => session_id}), do: Phoenix.PubSub.broadcast(PubSub, session_id, {:report_result, rows})


  def subscribe, do: Phoenix.PubSub.subscribe(PubSub, @topic)
  def subscribe(module), do: Phoenix.PubSub.subscribe(PubSub, module)

  defp generate_report(data) do
    %{"report-result" => get_filters_result(data["filters"]), "session_id" => data["session_id"]}
  end

  def call_task_supervisor(data), do: Supervisor.start_link([{AppTaskSupervisor, data}], strategy: :one_for_one)

end