defmodule FoodTrucks.Api do

  import FoodTrucks.Database.Query, only: [get: 2, get_one: 2, new: 1, remove: 2, update_keys: 4]
  alias FoodTrucks.{Database.Trucks}

  def get_filters_result(filters), do: get(Trucks,filters)

  def add_food_truck(truck), do: Map.merge(%Trucks{}, truck) |> new()

  def get_food_truck(location_id) when is_bitstring(location_id), do: String.to_integer(location_id) |> get_food_truck()
  def get_food_truck(location_id), do: get_one(Trucks,[locationid: location_id])

  def delete_food_truck(location_id) when is_bitstring(location_id), do: String.to_integer(location_id) |> delete_food_truck()
  def delete_food_truck(location_id), do: remove(Trucks,[locationid: location_id]) |> IO.inspect()

  def update_food_truck_m(location_id, data) when is_bitstring(location_id), do: String.to_integer(location_id) |> update_food_truck_m(data)
  def update_food_truck_m(location_id, data), do: update_keys(Trucks,[locationid: location_id], Map.keys(data), data)

end
